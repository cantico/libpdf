<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

function LibPdf_upgrade($version_base, $version_ini)
{
	$addonInfo = bab_getAddonInfosInstance('LibPdf');
	$addonInfo->registerFunctionality('TcPdf', 'func_tcpdf.class.php');

	$addonUpload = $addonInfo->getUploadPath();
	if (!is_dir($addonUpload)) {
		bab_mkdir($addonUpload);
	}

	return true;
}



function LibPdf_onDeleteAddon() {
    
    $addonInfo = bab_getAddonInfosInstance('LibPdf');
    $addonInfo->unregisterFunctionality('TcPdf');

	return true;
}